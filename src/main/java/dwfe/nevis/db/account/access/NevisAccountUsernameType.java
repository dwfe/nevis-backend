package dwfe.nevis.db.account.access;

public enum NevisAccountUsernameType
{
  EMAIL,
  PHONE,
  NICKNAME,
  ID,
}
