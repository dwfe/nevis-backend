package dwfe.nevis.db.other.country;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NevisCountryRepository extends JpaRepository<NevisCountry, String>
{
}
