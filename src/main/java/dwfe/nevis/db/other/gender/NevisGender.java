package dwfe.nevis.db.other.gender;

public enum NevisGender
{
  //
  // ATTENTION.
  // If one of the enumerations is already in use in database table entries,
  // then changing the order may cause the logic to malfunction
  //

  M,
  F
}
