package dwfe.nevis.test;

public enum NevisTestAuthType
{
  SIGN_IN,
  REFRESH
}
