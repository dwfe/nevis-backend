package dwfe.nevis.test;

public enum NevisTestResourceAccessingType
{
  USUAL,
  BAD_ACCESS_TOKEN
}
