package dwfe.nevis.test;

public enum NevisTestAuthorityLevel
{
  ANY,
  USER,
  ADMIN
}
