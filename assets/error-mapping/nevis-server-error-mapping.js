nevisServerErrorCodesMap = {
    'account-is-not-linked-to-email': 'Account is not linked to email',
    'account-is-not-linked-to-phone': 'Account is not linked to phone',
    'confirm-key-not-exist': 'Confirm key not exist',
    'dateOfBirth-cannot-be-parsed': 'Date cannot be parsed',
    'delay-between-duplicate-requests': 'Delay between requests. Try again later',
    'email-is-already-confirmed': 'Email is already confirmed',
    'email-not-exist': 'Email not exist',
    'email-present-in-database': 'Email present in database',
    'empty-confirm-key': 'Empty confirm key',
    'empty-country': 'Empty country',
    'empty-curpass': 'Empty curpass',
    'empty-dateOfBirth': 'Empty date of birth',
    'empty-email': 'Empty email',
    'empty-gender': 'Empty gender',
    'empty-google-response': 'Empty google response',
    'empty-newemail': 'Empty new email',
    'empty-newNickName': 'Empty new nickname',
    'empty-newpass': 'Empty new password',
    'empty-newphone': 'Empty new phone',
    'empty-nickName': 'Empty nickname',
    'empty-password': 'Empty password',
    'empty-phone': 'Empty phone',
    'empty-username': 'Empty username',
    'error-google-captcha-gateway': 'Error google captcha gateway',
    'exceeded-max100-email-length': 'Exceeded max100 email length',
    'exceeded-max100-nickName-length': 'Exceeded max100 nickname length',
    'exceeded-max100-phone-length': 'Exceeded max100 phone length',
    'google-captcha-detected-robot': 'Google captcha detected a robot',
    'id-must-be-a-positive-number': 'Id must be a positive number',
    'id-must-be-of-type-long': 'Id must be of type long',
    'id-not-exist': 'Id not exist',
    'id-present-in-database': 'Id present in database',
    'invalid-country': 'Invalid country',
    'invalid-email': 'Invalid email',
    'invalid-gender': 'Invalid gender',
    'linked-email-not-exist': 'Linked email not exist',
    'missing-confirm-key': 'Missing confirm key',
    'missing-curpass': 'Missing curpass',
    'missing-email': 'Missing email',
    'missing-google-response': 'Missing google response',
    'missing-newemail': 'Missing new email',
    'missing-newNickName': 'Missing new nickname',
    'missing-newpass': 'Missing new password',
    'missing-newphone': 'Missing new phone',
    'missing-password': 'Missing password',
    'missing-username': 'Missing username',
    'nickName-present-in-database': 'Nickname present in database',
    'no-email-associated-with-account': 'No email associated with account',
    'no-phone-associated-with-account': 'No phone associated with account',
    'out-of-range-min6-max55-newpass-length': 'Out of range min6 max55 new password length',
    'out-of-range-min6-max55-password-length': 'Out of range min6 max55 password length',
    'phone-present-in-database': 'Phone present in database',
    'the-generated-password-should-be-sent-somewhere': 'The generated password should be sent somewhere',
    'timeout-google-captcha-gateway': 'Timeout google captcha gateway',
    'username-must-be-passed': 'Username must be passed',
    'wrong-curpass': 'Wrong curpass'
};